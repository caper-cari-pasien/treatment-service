FROM gradle:jdk17-alpine
ARG PRODUCTION
ARG JDBC_DATABASE_PASSWORD
ARG JDBC_DATABASE_URL
ARG JDBC_DATABASE_USERNAME
ARG DB_SERVICE_URL

ENV PRODUCTION ${PRODUCTION}
ENV JDBC_DATABASE_PASSWORD ${JDBC_DATABASE_PASSWORD}
ENV JDBC_DATABASE_URL ${JDBC_DATABASE_URL}
ENV JDBC_DATABASE_USERNAME ${JDBC_DATABASE_USERNAME}
ENV DB_SERVICE_URL ${DB_SERVICE_URL}

WORKDIR /app
COPY ./build/libs/treatment-service-0.0.1-SNAPSHOT.jar /app
EXPOSE 8082
CMD ["java","-jar","treatment-service-0.0.1-SNAPSHOT.jar"]
