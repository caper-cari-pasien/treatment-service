package com.caper.treatmentservice.service;

import com.caper.treatmentservice.dto.AddCategoryDto;
import com.caper.treatmentservice.dto.AddOpenTreatmentDto;
import com.caper.treatmentservice.dto.OpenTreatmentDto;
import com.caper.treatmentservice.dto.TreatmentCategoryDto;
import com.caper.treatmentservice.repository.OpenTreatmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OpenTreatmentService {
    private final OpenTreatmentRepository openTreatmentRepository;

    public List<OpenTreatmentDto> findAll() {
        return openTreatmentRepository.findAll().block();
    }

    public OpenTreatmentDto addOpenTreatment(AddOpenTreatmentDto addOpenTreatmentDto) {
        return openTreatmentRepository.addOpenTreatment(addOpenTreatmentDto);
    }

    public OpenTreatmentDto findById(String treatmentId) {
        return openTreatmentRepository.findById(treatmentId).block();
    }

    public TreatmentCategoryDto findCategoryById(String categoryId) {
        return openTreatmentRepository.findCategoryById(categoryId);
    }

    public TreatmentCategoryDto addTreatmentCategory(AddCategoryDto addCategoryDto) {
        return openTreatmentRepository.addTreatmentCategory(addCategoryDto);
    }
}
