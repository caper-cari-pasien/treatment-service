package com.caper.treatmentservice.service;

import com.caper.treatmentservice.dto.AddRequestTreatmentDto;
import com.caper.treatmentservice.dto.RequestTreatmentDto;
import com.caper.treatmentservice.repository.RequestTreatmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RequestTreatmentService {
    private final RequestTreatmentRepository requestTreatmentRepository;

    public List<RequestTreatmentDto> findAll() {
        return requestTreatmentRepository.findAll();
    }

    public RequestTreatmentDto addRequest(AddRequestTreatmentDto addRequestTreatmentDto) {
        return requestTreatmentRepository.add(addRequestTreatmentDto);
    }
    public RequestTreatmentDto findById(String requestId) {
        return requestTreatmentRepository.findById(requestId).block();
    }
}
