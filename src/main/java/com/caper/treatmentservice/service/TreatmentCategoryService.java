package com.caper.treatmentservice.service;

import com.caper.treatmentservice.dto.TreatmentCategoryDto;
import com.caper.treatmentservice.repository.TreatmentCategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TreatmentCategoryService {
    private final TreatmentCategoryRepository treatmentCategoryRepository;

    public List<TreatmentCategoryDto> findAll() {
        return treatmentCategoryRepository.findAll();
    }
}
