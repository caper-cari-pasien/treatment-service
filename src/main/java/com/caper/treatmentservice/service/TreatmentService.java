package com.caper.treatmentservice.service;

import com.caper.treatmentservice.dto.*;
import com.caper.treatmentservice.exceptions.UserNotFoundException;
import com.caper.treatmentservice.repository.TreatmentRepository;
import com.caper.treatmentservice.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TreatmentService {
    private final TreatmentRepository treatmentRepository;
    private final UserRepository userRepository;

    public List<TreatmentDto> findAllTreatmentByUsername(String username) {
        var user = userRepository.findPatientByUsername(username);
        if(user != null) {
            return treatmentRepository.findAllByPatientUsername(username);
        }

        user = userRepository.findDoctorByUsername(username);
        if(user != null) {
            return treatmentRepository.findAllByDoctorUsername(username);
        }

        throw new UserNotFoundException(String.format("User %s not found", username));
    }

    public TreatmentDto verifyTreatment(String treatmentId) {
        return treatmentRepository.verifyTreatment(treatmentId);
    }

    public TreatmentDto addNote(String treatmentId, AddTreatmentNoteDto addTreatmentNoteDto) {
        return treatmentRepository.addNote(treatmentId, addTreatmentNoteDto);
    }

    public TreatmentDto registerTreatment(RegisterTreatmentDto registerTreatmentDto) {
        return treatmentRepository.registerTreatment(registerTreatmentDto);
    }
    public TreatmentDto findById(String treatmentId) {
        return treatmentRepository.findById(treatmentId).block();
    }}
