package com.caper.treatmentservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddRequestTreatmentDto {
    private String title;

    private String location;

    private Date treatmentTime;

    private String description;

    private String patientUsername;
}
