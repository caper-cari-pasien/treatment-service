package com.caper.treatmentservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TreatmentDto {
    private String id;

    private String note;

    @JsonProperty(value = "isVerified")
    private boolean isVerified;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    private OpenTreatmentDto openTreatment;

    private PatientDto patient;
}
