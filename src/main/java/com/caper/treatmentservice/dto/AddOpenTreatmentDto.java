package com.caper.treatmentservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddOpenTreatmentDto {
    private String location;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date treatmentTime;

    private String description;

    private Long price;

    private String categoryId;

    private String title;
    private String doctorUsername;
}
