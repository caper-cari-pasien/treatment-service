package com.caper.treatmentservice.dto;

public enum UserRoleDto {
    PATIENT, DOCTOR
}
