package com.caper.treatmentservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OpenTreatmentDto {
    private String id;

    private String location;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date treatmentTime;

    private String description;

    private Long price;

    private TreatmentCategoryDto category;
    private String title;

    private DoctorDto doctor;
}
