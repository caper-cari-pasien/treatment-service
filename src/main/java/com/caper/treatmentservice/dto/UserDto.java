package com.caper.treatmentservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private String id;

    private String username;

    private String nama;

    private UserRoleDto role;

    private String domisili;

    private int umur;
}
