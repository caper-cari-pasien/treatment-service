package com.caper.treatmentservice.repository;

import com.caper.treatmentservice.dto.AddRequestTreatmentDto;
import com.caper.treatmentservice.dto.RequestTreatmentDto;
import com.caper.treatmentservice.exceptions.WebClientNotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import java.util.List;

@Repository
public class RequestTreatmentRepository {
    private final WebClient client;

    public RequestTreatmentRepository(@Value("${db-service-url}") String baseUrl) {
        client = WebClient.create(baseUrl + "/api/treatment/pesan");
    }

    public List<RequestTreatmentDto> findAll() {
        return client.get().uri("").retrieve().bodyToFlux(RequestTreatmentDto.class).collectList().block();
    }

    public RequestTreatmentDto add(AddRequestTreatmentDto addRequestTreatmentDto) {
        try{
            return client.post().uri("").body(Mono.just(addRequestTreatmentDto), AddRequestTreatmentDto.class)
                    .retrieve().bodyToMono(RequestTreatmentDto.class).block();
        }catch (WebClientResponseException e) {
            if(e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new WebClientNotFoundException("Invalid patient");
            }
            throw e;
        }

    }
    public Mono<RequestTreatmentDto> findById(String requestId) {
        try{
            return client.get().uri("/" + requestId).retrieve().bodyToMono(RequestTreatmentDto.class);
        }catch (WebClientResponseException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return null;
            }
            throw e;
        }
    }
}
