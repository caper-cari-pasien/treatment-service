package com.caper.treatmentservice.repository;

import com.caper.treatmentservice.dto.PatientDto;
import com.caper.treatmentservice.dto.RegisterTreatmentDto;
import com.caper.treatmentservice.dto.TreatmentDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

@Repository
public class UserRepository {
    private final WebClient client;

    public UserRepository(@Value("${db-service-url}") String baseUrl) {
        client = WebClient.create(baseUrl + "/api/user");
    }

    public PatientDto findPatientByUsername(String username) {
        try{
            return client.get().uri("/profile/pasien/" + username)
                    .retrieve().bodyToMono(PatientDto.class).block();
        }catch (WebClientResponseException e) {
            if(e.getStatusCode().equals(HttpStatus.NOT_FOUND)){
                return null;
            }

            throw e;
        }
    }

    public PatientDto findDoctorByUsername(String username) {
        try{
            return client.get().uri("/profile/dokter/" + username)
                    .retrieve().bodyToMono(PatientDto.class).block();
        }catch (WebClientResponseException e) {
            if(e.getStatusCode().equals(HttpStatus.NOT_FOUND)){
                return null;
            }

            throw e;
        }
    }
}
