package com.caper.treatmentservice.repository;

import com.caper.treatmentservice.dto.*;
import com.caper.treatmentservice.exceptions.WebClientNotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import java.util.List;

@Repository
public class OpenTreatmentRepository {

    private final WebClient client;

    public OpenTreatmentRepository(@Value("${db-service-url}") String baseUrl) {
        client = WebClient.create(baseUrl + "/api/treatment");
    }

    public Mono<List<OpenTreatmentDto>> findAll() {
        try {
            return client.get().uri("").retrieve().bodyToFlux(OpenTreatmentDto.class).collectList();
        } catch (WebClientResponseException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return null;
            }
            throw e;
        }
    }

    public OpenTreatmentDto addOpenTreatment(AddOpenTreatmentDto addOpenTreatmentDto) {
        try{
            return client.post().uri("").body(Mono.just(addOpenTreatmentDto), AddOpenTreatmentDto.class)
                    .retrieve().bodyToMono(OpenTreatmentDto.class).block();
        }catch (WebClientResponseException e) {
            if(e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new WebClientNotFoundException("Invalid doctor or category id");
            }
            throw e;
        }
    }

    public Mono<OpenTreatmentDto> findById(String treatmentId) {
        try{
            return client.get().uri("/" + treatmentId).retrieve().bodyToMono(OpenTreatmentDto.class);
        }catch (WebClientResponseException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return null;
            }
            throw e;
        }
    }

    public TreatmentCategoryDto findCategoryById(String id) {
        try{
            return client.get().uri("/category/" + id).retrieve().bodyToMono(TreatmentCategoryDto.class).block();
        }catch (WebClientResponseException e) {
            if(e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new WebClientNotFoundException("Invalid treatment category id");
            }
            throw e;
        }
    }

    public TreatmentCategoryDto addTreatmentCategory(AddCategoryDto addCategoryDto) {
        return client.post().uri("/category").body(Mono.just(addCategoryDto), AddCategoryDto.class)
                .retrieve().bodyToMono(TreatmentCategoryDto.class).block();
    }
}
