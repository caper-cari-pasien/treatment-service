package com.caper.treatmentservice.repository;

import com.caper.treatmentservice.dto.TreatmentCategoryDto;
import com.caper.treatmentservice.exceptions.WebClientNotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import java.util.List;

@Repository
public class TreatmentCategoryRepository {
    private final WebClient client;

    public TreatmentCategoryRepository(@Value("${db-service-url}") String baseUrl) {
        client = WebClient.create(baseUrl + "/api/treatment");
    }
    public List<TreatmentCategoryDto> findAll() {
        try{
            return client.get().uri("/category").retrieve().bodyToFlux(TreatmentCategoryDto.class).collectList().block();
        }catch (WebClientResponseException e) {
            if(e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new WebClientNotFoundException("Cant get all treatment category");
            }
            throw e;
        }
    }}
