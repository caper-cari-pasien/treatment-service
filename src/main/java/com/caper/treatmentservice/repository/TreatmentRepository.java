package com.caper.treatmentservice.repository;

import com.caper.treatmentservice.dto.AddTreatmentNoteDto;
import com.caper.treatmentservice.dto.RegisterTreatmentDto;
import com.caper.treatmentservice.dto.TreatmentDto;
import com.caper.treatmentservice.exceptions.WebClientNotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TreatmentRepository {
    private final WebClient client;

    public TreatmentRepository(@Value("${db-service-url}") String baseUrl) {
        client = WebClient.create(baseUrl + "/api/treatment/berjalan");
    }

    public List<TreatmentDto> findAllByDoctorUsername(String username) {
        try {
            return client.get().uri("/doctor/" + username).retrieve().bodyToFlux(TreatmentDto.class).collectList().block();
        } catch (WebClientResponseException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return new ArrayList<>();
            }
            throw e;
        }
    }

    public List<TreatmentDto> findAllByPatientUsername(String username) {
        try {
            return client.get().uri("/patient/" + username).retrieve().bodyToFlux(TreatmentDto.class).collectList().block();
        } catch (WebClientResponseException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new WebClientNotFoundException("Invalid id");
            }
            throw e;
        }
    }

    public TreatmentDto verifyTreatment(String treatmentId) {
        try {
            return client.patch().uri("/verify/" + treatmentId).retrieve().bodyToMono(TreatmentDto.class).block();
        } catch (WebClientResponseException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                System.out.println(e);
                return null;
            }
            throw e;
        }
    }

    public TreatmentDto addNote(String treatmentId, AddTreatmentNoteDto addTreatmentNoteDto) {
        try {
            return client.patch().uri("/" + treatmentId + "/note").body(Mono.just(addTreatmentNoteDto), AddTreatmentNoteDto.class)
                    .retrieve().bodyToMono(TreatmentDto.class).block();
        } catch (WebClientResponseException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                System.out.println(e);
                return null;
            }
            throw e;
        }
    }

    public TreatmentDto registerTreatment(RegisterTreatmentDto registerTreatmentDto) {
        try{
            return client.post().uri("").body(Mono.just(registerTreatmentDto), RegisterTreatmentDto.class)
                    .retrieve().bodyToMono(TreatmentDto.class).block();
        }catch (WebClientResponseException e) {
            if(e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new WebClientNotFoundException("Invalid user or treatment id");
            }
            throw e;
        }
    }
    public Mono<TreatmentDto> findById(String treatmentId) {
        try{
            return client.get().uri("/" + treatmentId).retrieve().bodyToMono(TreatmentDto.class);
        }catch (WebClientResponseException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return null;
            }
            throw e;
        }
    }
}
