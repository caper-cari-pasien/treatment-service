package com.caper.treatmentservice.exceptions;

public class WebClientNotFoundException extends RuntimeException{
    public WebClientNotFoundException(String message){
        super(message);
    }
}
