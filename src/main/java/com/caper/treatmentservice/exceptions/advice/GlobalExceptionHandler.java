package com.caper.treatmentservice.exceptions.advice;

import com.caper.treatmentservice.exceptions.UserNotFoundException;
import com.caper.treatmentservice.exceptions.WebClientNotFoundException;
import com.caper.treatmentservice.exceptions.ErrorTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = { WebClientNotFoundException.class, UserNotFoundException.class })
    public ResponseEntity<Object> dbNotFound(Exception exception) {
        HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        ErrorTemplate baseException = new ErrorTemplate(
                exception.getMessage(),
                badRequest);

        exception.printStackTrace();
        return new ResponseEntity<>(baseException, badRequest);
    }

    @ExceptionHandler(value = { Exception.class })
    public ResponseEntity<Object> generalError(Exception exception) {
        HttpStatus badRequest = HttpStatus.INTERNAL_SERVER_ERROR;
        ErrorTemplate baseException = new ErrorTemplate(
                exception.getMessage(),
                badRequest);

        exception.printStackTrace();
        return new ResponseEntity<>(baseException, badRequest);
    }

}
