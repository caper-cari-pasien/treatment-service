package com.caper.treatmentservice.controller;

import com.caper.treatmentservice.dto.AddCategoryDto;
import com.caper.treatmentservice.dto.AddOpenTreatmentDto;
import com.caper.treatmentservice.dto.OpenTreatmentDto;
import com.caper.treatmentservice.dto.TreatmentCategoryDto;
import com.caper.treatmentservice.service.OpenTreatmentService;
import com.caper.treatmentservice.service.TreatmentCategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class OpenTreatmentController {
    private final OpenTreatmentService openTreatmentService;
    private final TreatmentCategoryService treatmentCategoryService;

    @GetMapping("")
    public ResponseEntity<List<OpenTreatmentDto>> getAllOpenTreatment() {
        List<OpenTreatmentDto> response = openTreatmentService.findAll();
        return ResponseEntity.ok(response);
    }

    @PostMapping("")
    public ResponseEntity<OpenTreatmentDto> add(@RequestBody AddOpenTreatmentDto addOpenTreatmentDto) {
        return ResponseEntity.ok(openTreatmentService.addOpenTreatment(addOpenTreatmentDto));
    }

    @GetMapping("/{treatmentId}")
    public ResponseEntity<OpenTreatmentDto> getOpenTreatmentById(@PathVariable String treatmentId) {
        OpenTreatmentDto response = openTreatmentService.findById(treatmentId);
        return ResponseEntity.ok(response);
    }
    @GetMapping("/category")
    public ResponseEntity<List<TreatmentCategoryDto>> getAllCategory() {
        List<TreatmentCategoryDto> response = treatmentCategoryService.findAll();
        return ResponseEntity.ok(response);
    }
    @GetMapping("/category/{categoryId}")
    public ResponseEntity<TreatmentCategoryDto> getCategoryById(@PathVariable String categoryId) {
        System.out.println("Tes123");
        return ResponseEntity.ok(openTreatmentService.findCategoryById(categoryId));
    }

    @PostMapping("/category")
    public ResponseEntity<TreatmentCategoryDto> addTreatmentCategory(@RequestBody AddCategoryDto addCategoryDto) {
        return ResponseEntity.ok(openTreatmentService.addTreatmentCategory(addCategoryDto));
    }


}
