package com.caper.treatmentservice.controller;

import com.caper.treatmentservice.dto.AddTreatmentNoteDto;
import com.caper.treatmentservice.dto.RegisterTreatmentDto;
import com.caper.treatmentservice.dto.TreatmentDto;
import com.caper.treatmentservice.service.TreatmentService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/berjalan")
@RequiredArgsConstructor
public class TreatmentController {
    private final TreatmentService treatmentService;

    @GetMapping("")
    public ResponseEntity<List<TreatmentDto>> getAllTreatmentBerjalan(HttpServletRequest request) {
        var username = request.getHeader("username");
        var response = treatmentService.findAllTreatmentByUsername(username);
        return ResponseEntity.ok(response);
    }
    @GetMapping("/{treatmentId}")
    public ResponseEntity<TreatmentDto> getTreatmentBerjalanById(@PathVariable String treatmentId) {
        TreatmentDto response = treatmentService.findById(treatmentId);
        return ResponseEntity.ok(response);
    }
    @PostMapping("/register")
    public ResponseEntity<TreatmentDto> registerTreatment(@RequestBody RegisterTreatmentDto registerTreatmentDto) {
        var response = treatmentService.registerTreatment(registerTreatmentDto);
        return ResponseEntity.ok(response);
    }

    @PatchMapping("/verify/{treatmentId}")
    public ResponseEntity<TreatmentDto> verifyTreatment(@PathVariable String treatmentId) {
        var response = treatmentService.verifyTreatment(treatmentId);
        return ResponseEntity.ok(response);
    }

    @PatchMapping("/{treatmentId}/note")
    public ResponseEntity<TreatmentDto> addNote(@PathVariable String treatmentId, @RequestBody AddTreatmentNoteDto addTreatmentNoteDto) {
        var response = treatmentService.addNote(treatmentId, addTreatmentNoteDto);
        return ResponseEntity.ok(response);
    }
}
