package com.caper.treatmentservice.controller;

import com.caper.treatmentservice.dto.AddRequestTreatmentDto;
import com.caper.treatmentservice.dto.RequestTreatmentDto;
import com.caper.treatmentservice.service.RequestTreatmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/pesan")
@RequiredArgsConstructor
public class RequestTreatmentController {
    private  final RequestTreatmentService requestTreatmentService;

    @GetMapping("")
    public ResponseEntity<List<RequestTreatmentDto>> findAll() {
        return ResponseEntity.ok(requestTreatmentService.findAll());
    }

    @PostMapping("")
    public ResponseEntity<RequestTreatmentDto> add(@RequestBody AddRequestTreatmentDto addRequestTreatmentDto) {
        var responseBody = requestTreatmentService.addRequest(addRequestTreatmentDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(responseBody);
    }
    @GetMapping("/{requestId}")
    public ResponseEntity<RequestTreatmentDto> getRequestTreatmentById(@PathVariable String requestId) {
        RequestTreatmentDto response = requestTreatmentService.findById(requestId);
        return ResponseEntity.ok(response);
    }
}
